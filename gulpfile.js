var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    nunjucksRender = require('gulp-nunjucks-render'),
    autoprefixer = require('gulp-autoprefixer'),
    del = require('del');


// Static server
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "dist/"
        }
    });
});


//html
gulp.task('html', function () {
    return gulp.src('src/*.html')
        .pipe(sourcemaps.init())
        .pipe(nunjucksRender({
            path: ['src/template/'] // String or Array
        }))
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.stream());
});




//css
gulp.task('css', function () {
    gulp.src(['src/stylesheets/**/*.*'])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 5 version', 'ie 9'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
});


//img
gulp.task('img', function () {
    gulp.src('src/images/**/*.*')
        .pipe(gulp.dest('dist/images'))
        .pipe(browserSync.stream());
});


//js
gulp.task('js', function () {
    gulp.src('src/js/**/*.*')
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
});


//fonts
gulp.task('fonts', function () {
    gulp.src('src/fonts/**/*.*')
        .pipe(gulp.dest('dist/fonts/'))
        .pipe(browserSync.stream());
});



// clean
gulp.task('clean', function () {
    return del('dist');
});


// watch
gulp.task('watch', function () {
    gulp.watch('src/*.html', ['html']);
    gulp.watch('src/stylesheets/**/*.scss', ['css']);
    gulp.watch('src/js/*.js', ['js']);
    gulp.watch('src/img/**', ['img']);
    gulp.watch('src/fonts/**', ['fonts']);
});


//default
gulp.task('default', ['browser-sync','js', 'html', 'css', 'img',  'fonts',  'watch']);
